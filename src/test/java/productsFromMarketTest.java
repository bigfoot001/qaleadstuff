import enums.marketsForPai;
import enums.statusesOfPai;
import functions.api.productsFromMarket;
import org.junit.Test;
import pojo.paiStatus;
import pojo.paiValidation;

public class productsFromMarketTest {
    private productsFromMarket pfm = new productsFromMarket();
    private String market = marketsForPai.england.label;

    @Test
    public void testGetProductsWithStatus() throws Exception{
        pfm.getProductsWithStatus(
                pfm.getProductListForPaiTests(pfm.getProductsWithPaiStatus(marketsForPai.italy.label),
                        pfm.getValidatedProducts(market, "202011")),
                statusesOfPai.Green.label);
    }

    @Test
    public void testGetProductsForPaiTests() throws Exception{
        pfm.getProductListForPaiTests(pfm.getProductsWithPaiStatus(market), pfm.getValidatedProducts(market, "202011"));
    }

    @Test
    public void testGetProductsWithPaiStatus() throws Exception{
        paiStatus[] paiStatusArray = pfm.getProductsWithPaiStatus(market);
        System.out.println();
    }
    @Test
    public void testGetValidatedProducts() throws Exception{
        paiValidation[] paiValidationArray = pfm.getValidatedProducts(market, "202011");
        System.out.println();
    }
}