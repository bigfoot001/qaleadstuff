package functions.tools;

import enums.marketsForPai;
import functions.api.productsFromMarket;
import org.junit.Test;

import java.io.IOException;

public class createExcelFromJsonTest {
    private createExcelFromJson efp = new createExcelFromJson();
    private productsFromMarket pfm = new productsFromMarket();
    private String market = marketsForPai.england.label;


    @Test
    public void createExcelTestProducts() throws IOException {
        efp.createExcelTestProducts(
                pfm.getProductListForPaiTests(pfm.getProductsWithPaiStatus(market), pfm.getValidatedProducts(market, "202011")));
    }
}