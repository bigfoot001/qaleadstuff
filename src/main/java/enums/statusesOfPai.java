package enums;

public enum statusesOfPai {
    Green("GRN"),
    Red("RED"),
    Yellow("YLW"),
    Grey("S");

    public final String label;

    statusesOfPai(String label) {
        this.label = label;
    }
}
