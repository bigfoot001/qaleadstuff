package enums;

public enum marketsForPai {
    italy("IT/IT"),
    england("UK/EN"),
    poland("PL/PL"),
    russia("RU/RU");

    public final String label;

    marketsForPai(String label) {
        this.label = label;
    }
}
