package functions.tools;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import pojo.paiProducts;
import pojo.paiStatus;
import pojo.paiValidation;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class createExcelFromJson {
    private String[] columns;
    private Workbook workBook = new XSSFWorkbook();
    CreationHelper creationHelper = workBook.getCreationHelper();

    // create XLSX for all of the validated products with PAI status
    public void createExcelTestProducts (paiProducts[] arrayTestProducts) throws IOException {
        columns = new String[]{"Fsc", "cmFsc", "LineNr", "PAI Status"};
        Sheet sheet = workBook.createSheet("testProducts");
        createHeader(sheet);
        //fill data
        int rowNum = 1;
        for (paiProducts paiProducts : arrayTestProducts ){
            Row dataRow = sheet.createRow(rowNum++);
            dataRow.createCell(0).setCellValue(paiProducts.getFsc());
            dataRow.createCell(1).setCellValue(paiProducts.getCmFsc().toString());
            dataRow.createCell(2).setCellValue(paiProducts.getLineNr().toString());
            dataRow.createCell(3).setCellValue(paiProducts.getPai());
        }
        //set size of columns
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        createFile();
    }

    // create XLSX for all products from status request
//    public void createExcelAllProducts (paiStatus[] arrayAllProducts) {
//
//    }

    // create XLSX for all validated products without PAI status
//    public void createExcelValidatedProducts (paiValidation[] arrayValidatedProducts) {
//
//    }

    private Sheet createHeader (Sheet sheet){
        Font headerFont = workBook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        CellStyle headerCellStyle = workBook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setLocked(true);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        Row headerRow = sheet.createRow(0);
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        return sheet;
    }

    private void createFile () throws IOException {
        //create file
        FileOutputStream fileOut = new FileOutputStream("poi-generated-file.xlsx");
        workBook.write(fileOut);
        fileOut.close();
        // Closing the workbook
        workBook.close();
    }

}
