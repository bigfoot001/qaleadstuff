package functions.api;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.gson.Gson;
import pojo.paiProducts;
import pojo.paiStatus;
import pojo.paiValidation;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;

public class productsFromMarket {
    private Gson gsonTool = new Gson();

    public paiProducts[] getProductsWithStatus(paiProducts[] productListForPaiTests, String paiStatus) {
        paiProducts[] productsWithStatus = new paiProducts[productListForPaiTests.length];
        int counter = 0;
        for (int i = 0; i < productListForPaiTests.length; i++) {
            while (productListForPaiTests[i].getPai().compareTo(paiStatus) != 0) {
                i++;
            }
            if (productListForPaiTests[i].getPai().compareTo(paiStatus) == 0) {
                productsWithStatus[counter] = productListForPaiTests[i];
                counter++;
            }
        }
        paiProducts[] productsFinalWithStatus = new paiProducts[counter];
        System.arraycopy(productsWithStatus,0,productsFinalWithStatus,0,counter);
        return productsFinalWithStatus;
    }



    public paiProducts[] getProductListForPaiTests(paiStatus[] productsWithPaiStatus, paiValidation[] validatedProducts) {
        Arrays.sort(productsWithPaiStatus, new Comparator<paiStatus>() {
            @Override
            public int compare(paiStatus paiStatus1, paiStatus paiStatus2) {
                return paiStatus1.fsc.compareTo(paiStatus2.fsc);
            }
        });
        Arrays.sort(validatedProducts, new Comparator<paiValidation>() {
            @Override
            public int compare(paiValidation paiValidation1, paiValidation paiValidation2) {
                return paiValidation1.fsc.compareTo(paiValidation2.fsc);
            }
        });
        paiProducts[] products = new paiProducts[validatedProducts.length];
        int validation = 0;
        int product = 0;
        for (int status = 0; status < productsWithPaiStatus.length; status++) {
            if (validation < validatedProducts.length) {
                while (productsWithPaiStatus[status].fsc > validatedProducts[validation].fsc) {
                    if (validation < validatedProducts.length - 1) {
                        validation++;
                    } else {
                        paiProducts[] productsFinal = new paiProducts[product];
                        System.arraycopy(products,0,productsFinal,0,product);
                        return productsFinal;
                    }
                }
            }
            while (validatedProducts[validation].fsc > productsWithPaiStatus[status].fsc) {
                if (status < productsWithPaiStatus.length - 1) {
                    status++;
                } else {
                    paiProducts[] productsFinal = new paiProducts[product];
                    System.arraycopy(products,0,productsFinal,0,product);
                    return productsFinal;
                }
            }

            if (productsWithPaiStatus[status].fsc.compareTo(validatedProducts[validation].fsc) == 0) {
                products[product] = new paiProducts(productsWithPaiStatus[status].fsc, productsWithPaiStatus[status].cmFsc, productsWithPaiStatus[status].pai, validatedProducts[validation].lineNr);
                if (validation < validatedProducts.length - 1) {
                    validation++;
                }
                if (product < products.length - 1) {
                    product++;
                }
            }
        }

        paiProducts[] productsFinal = new paiProducts[product];
        System.arraycopy(products,0,productsFinal,0,product);
        return productsFinal;
    }

    public paiStatus[] getProductsWithPaiStatus(String market) throws IOException {
        String url = "";
        if ( market.equalsIgnoreCase("IT/IT") ||
                market.equalsIgnoreCase("UK/EN")) {
            url = "https://choiceservicesqaf.avon.com/myavon/product/v1/rest/" + market + "/pai4customers";
        }
        if ( market.equalsIgnoreCase("PL/PL") ||
                market.equalsIgnoreCase("RU/RU"))   {
            url = "https://choiceservicesqaf.avon.com/myavon/product/v1/rest/" + market + "/pai?date="+ LocalDate.now().toString();

        }
        return gsonTool.fromJson(request(url).execute().parseAsString(), paiStatus[].class);
    }

    public paiValidation[] getValidatedProducts(String market, String campaign) throws IOException {
        String url = "https://choiceservicesqaf.avon.com/myavon/product/v1/rest/" + market + "/lineNumbers?cmpgnId=" + campaign;
        return gsonTool.fromJson(request(url).execute().parseAsString(), paiValidation[].class);
    }

    private HttpRequest request(String url) throws IOException {

        HttpRequestFactory requestFactory = new NetHttpTransport().createRequestFactory();
        HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
        HttpHeaders headerPaiStatus = new HttpHeaders();
        headerPaiStatus.set("Accept", "application/json; charset=UTF-8");
        headerPaiStatus.set("X-Api-Key", "vAbgNMScsFzcdP3Psc2fF5pDLuFZNXqF");
        request.setHeaders(headerPaiStatus);

        return request;
    }


}
