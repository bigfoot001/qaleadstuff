package pojo;

public class paiValidation {
    public Integer lineNr;
    public Integer fsc;

    public paiValidation(Integer lineNr, Integer fsc) {
        this.lineNr = lineNr;
        this.fsc = fsc;
    }

    public Integer getFsc() {
        return fsc;
    }

    public Integer getLineNr() {
        return lineNr;
    }

}
