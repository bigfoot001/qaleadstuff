package pojo;

public class paiProducts {
    private Integer fsc;
    private Integer cmFsc;
    private String pai;
    private Integer lineNr;

    public paiProducts(Integer fsc, Integer cmFsc, String pai, Integer lineNr) {
        this.fsc = fsc;
        this.cmFsc = cmFsc;
        this.pai = pai;
        this.lineNr = lineNr;
    }

    public Integer getFsc() {
        return fsc;
    }

    public void setFsc(Integer fsc) {
        this.fsc = fsc;
    }

    public Integer getCmFsc() {
        return cmFsc;
    }

    public void setCmFsc(Integer cmFsc) {
        this.cmFsc = cmFsc;
    }

    public String getPai() {
        return pai;
    }

    public void setPai(String pai) {
        this.pai = pai;
    }

    public Integer getLineNr() {
        return lineNr;
    }

    public void setLineNr(Integer lineNr) {
        this.lineNr = lineNr;
    }

}
