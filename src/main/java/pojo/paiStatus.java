package pojo;

public class paiStatus {
    public Integer fsc;
    public Integer cmFsc;
    public String pai;

    public paiStatus(Integer fsc, Integer cmFsc, String pai) {
        this.fsc = fsc;
        this.cmFsc = cmFsc;
        this.pai = pai;
    }

    public Integer getFsc() {
        return fsc;
    }

    public Integer getCmFsc() {
        return cmFsc;
    }

    public String getPai() {
        return pai;
    }

}
